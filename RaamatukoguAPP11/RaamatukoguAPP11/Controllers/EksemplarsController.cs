﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;
using System.IO;
using Newtonsoft.Json;
using PagedList;


namespace RaamatukoguAPP11
{
    public partial class Eksemplar
    {
        public bool KasSaadaval
        {
            get => Saadaval == 1;
            set => Saadaval = value ? 1 : 0;
        }

        string _Isbn = "";
        string _Tiitel = "";
        string _Autor = "";
        string _Aasta = "";
        byte[] _Pilt = null;

        int _Kategooria = 0;

        public string Isbn//Raamatu loomiseks vajalik property, et eksemplari kaudu saaks raamatu mudelile ligi
        {
            get => RaamatuKood == 0 ? _Isbn : Raamat.Isbn;
            set => _Isbn = value;
        }
        public string UIsbn => _Isbn;//Raamatu muutmiseks vajalik property (U ehk uus)

        public string Tiitel
        {
            get => RaamatuKood == 0 ? _Tiitel : Raamat.Tiitel;
            set => _Tiitel = value;
        }
        public string UTiitel => _Tiitel;
        public string Autor
        {
            get => RaamatuKood == 0 ? _Autor : Raamat.Autor;
            set => _Autor = value;
        }
        public string UAutor => _Autor;
        public string Aasta
        {
            get => RaamatuKood == 0 ? _Aasta : Raamat.Aasta;
            set => _Aasta = value;
        }
        public string UAasta => _Aasta;
        public byte[] Pilt
        {
            get => RaamatuKood == 0 ? _Pilt : Raamat.Pilt;
            set => _Pilt = value;
        }
        public byte[] UPilt => _Pilt;

        public int Kategooria
        {
            get => RaamatuKood == 0 ? _Kategooria : Raamat.Kategooria.Kood;
            set => _Kategooria = value;
        }
        public int UKategooria => _Kategooria;
    }
}
namespace RaamatukoguAPP11.Controllers
{
    public class EksemplarsController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();

        public ActionResult EksPilt(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Eksemplar e = db.Eksemplar.Find(id.Value);
            if (e == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (e.Pilt?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var pilt = e.Pilt.ToArray();
            return File(pilt, "image/jpg");
        }

        [Authorize]
        // GET: Eksemplars
        public ActionResult Index(string sortOrder, string currentFilter, string searching, int? page)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            var eksemplar = db.Eksemplar.Include(e => e.KasutajaH).Include(e => e.Raamat).Include(e => e.KasutajaO).ToList().Where(y => y.Omanik == k.Kood);

            ViewBag.CurrentSort = sortOrder;
            ViewBag.TiitliSort = String.IsNullOrEmpty(sortOrder) ? "Tiitel_desc" : "";
            ViewBag.AutoriSort = String.IsNullOrEmpty(sortOrder) ? "Autor_desc" : "";
            ViewBag.KategooriaSort = String.IsNullOrEmpty(sortOrder) ? "Kategooria_desc" : "";

            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }
            ViewBag.CurrentFilter = searching;

            if (Kasutaja.ByEmail(User.Identity.Name) != null)
            {

                if (!String.IsNullOrEmpty(searching))
                {
                    eksemplar = eksemplar
                        .Where(x => x.Raamat.Tiitel.ToUpper().Contains(searching.ToUpper())
                        || x.Raamat.Autor.ToUpper().Contains(searching.ToUpper())
                        || x.Raamat.Kategooria.Kategooria1.ToUpper().Contains(searching.ToUpper())
                        /*|| searching == null*/);
                }

                switch (sortOrder)
                {
                    case "Tiitel_desc":
                        eksemplar = eksemplar.OrderByDescending(e => e.Raamat.Tiitel);
                        break;
                    case "Autor":
                        eksemplar = eksemplar.OrderBy(e => e.Raamat.Autor);
                        break;
                    case "Autor_desc":
                        eksemplar = eksemplar.OrderByDescending(e => e.Raamat.Autor);
                        break;
                    case "Kategooria":
                        eksemplar = eksemplar.OrderBy(e => e.Raamat.Kategooria.Kategooria1);
                        break;
                    case "Kategooria_desc":
                        eksemplar = eksemplar.OrderByDescending(e => e.Raamat.Kategooria.Kategooria1);
                        break;
                    default:
                        eksemplar = eksemplar.OrderBy(e => e.Raamat.Tiitel);
                        break;
                }

                int pageSize = 3;
                int pageNumber = (page ?? 1);
                return View(eksemplar.ToPagedList(pageNumber, pageSize));
            }


            else
                return RedirectToAction("Index", "Home");
        }

        [Authorize]
        // GET: Eksemplars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            if (eksemplar == null)
            {
                return HttpNotFound();
            }

            return View(eksemplar);
        }

        [Authorize]
        // GET: Eksemplars/Create/id
        public ActionResult Create(long? id)
        {
            ViewBag.Raamatud = db.Raamat.ToList();
            Raamat r = db.Raamat.Find(id ?? -1);
            Eksemplar e = new Eksemplar();
            if (id != null) { e.Isbn = "x" + id.ToString(); } // genereerib ISBN koodi, GenereeriIsbnEks meetodi abil. Tundub, et id ei jookse konflikti. Kui midagi juhtub, siis seetõttu, et GenereeriIsbnEks annab id-ks uue väärtuse. 
      
            if (r == null)
            {
                ViewBag.RaamatuKood = new SelectList(db.Raamat.OrderBy(x=>x.Tiitel), "Kood", "Tiitel");//dropdown list on tähestiku järjekorras
            }
            else
            {
                e.RaamatuKood = r.Kood;
                ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "Tiitel", e.RaamatuKood);
            }

            ViewBag.Kategooria = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1");

            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            ViewBag.Hoidja = new SelectList(db.Kasutaja, "Kood", "Email", ka.Kood);
            ViewBag.Omanik = new SelectList(db.Kasutaja, "Kood", "Email", ka.Kood);

            Silt s = db.Silt.Find(ka.Kood);
            ViewBag.Sildid = db.Silt.Where(x => x.KasutajaKood == ka.Kood).ToList();

            return View("Create",e);
        }
        
        public ActionResult GenereeriIsbnEks()
        {            
            Random random = new Random();
            string r = "";
            for (int i = 1; i < 13; i++)
            {
                r += random.Next(0, 9).ToString();
            }
            return Create((long.Parse(r)));
        }

        [Authorize]
        // POST: Eksemplars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,RaamatuKood,Isbn,Tiitel,Autor,Aasta,Pilt,Kategooria,Omanik,Hoidja,AsukohaKommentaar,Kapp,Riiul,Saadaval,KasSaadaval")] Eksemplar eksemplar, HttpPostedFileBase file)
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            eksemplar.Omanik = ka.Kood;

            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        eksemplar.Pilt = buff;
                    }
                }

                db.Eksemplar.Add(eksemplar);
                if (eksemplar.RaamatuKood == 0)
                {
                    Raamat u = db.Raamat.Where(x => x.Tiitel == eksemplar.Tiitel).Take(1).SingleOrDefault();
                    if (u == null)
                    {
                        Kategooria k = db.Kategooria.Where(y => y.Kood == eksemplar.Kategooria).Take(1).SingleOrDefault();
                        
                        db.Raamat.Add(u = new Raamat { Tiitel = eksemplar.Tiitel, Autor = eksemplar.Autor, Isbn=eksemplar.Isbn, Aasta=eksemplar.Aasta, Pilt=eksemplar.Pilt, KategooriaKood=k.Kood});
                        db.SaveChanges();
                    }
                    u.Eksemplar.Add(eksemplar);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.Kategooria = new SelectList(db.Kategooria, "Kood", "Kategooria1", eksemplar.Kategooria);
            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "Tiitel", eksemplar.RaamatuKood);
            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "RaamatuKood", eksemplar.RaamatuKood);
            //TODO: Üks ülemistest viewbagidest on ülearune

            ViewBag.Hoidja = new SelectList(db.Kasutaja, "Kood", "Email", ka.Kood);
            ViewBag.Omanik = new SelectList(db.Kasutaja, "Kood", "Email", ka.Kood);
            return View(eksemplar);
        }

        [Authorize]
        // GET: Eksemplars/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            if (eksemplar == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.Raamatud = db.Raamat.ToList();
            
            Raamat r = db.Raamat.Find(eksemplar.RaamatuKood);
            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "Tiitel", r.Kood);
            ViewBag.Kategooria = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", r.KategooriaKood);

            Silt s = db.Silt.Find(eksemplar.Omanik);
            ViewBag.Sildid = db.Silt.Where(x=> x.KasutajaKood==eksemplar.Omanik).ToList();
            
            ViewBag.Hoidja = new SelectList(db.Kasutaja, "Kood", "Email", eksemplar.Omanik);
            ViewBag.Omanik = new SelectList(db.Kasutaja, "Kood", "Email", eksemplar.Omanik);
            return View(eksemplar);
        }

        [Authorize]
        // POST: Eksemplars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,RaamatuKood,Isbn,Tiitel,Autor,Aasta,Pilt,Kategooria,Omanik,Hoidja,AsukohaKommentaar,Kapp,Riiul,Saadaval,KasSaadaval")] Eksemplar eksemplar, HttpPostedFileBase file)
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            eksemplar.Omanik = ka.Kood;

            if (ModelState.IsValid)
            {
                //eksemplar.RaamatuKood = eksemplar.Raamat.Kood;
                db.Entry(eksemplar).State = EntityState.Modified;
                db.SaveChanges();
                
                Raamat u = db.Raamat.Where(x => x.Kood == eksemplar.RaamatuKood).Take(1).SingleOrDefault();
                u.Isbn = eksemplar.UIsbn;
                u.Tiitel = eksemplar.UTiitel;
                u.Autor = eksemplar.UAutor;
                u.Aasta = eksemplar.UAasta;
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        eksemplar.Pilt = buff;
                    }
                }
                else eksemplar.Pilt = u.Pilt;
                u.Pilt = eksemplar.UPilt;
                u.KategooriaKood = eksemplar.UKategooria;

                // TODO: siit tuleb saata tulevikus teavitus moderaatorile

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "Tiitel", eksemplar.RaamatuKood);
            ViewBag.Kategooria = new SelectList(db.Kategooria, "Kood", "Kategooria1", eksemplar.Kategooria);
            ViewBag.Hoidja = new SelectList(db.Kasutaja, "Kood", "Email", eksemplar.Hoidja);
            ViewBag.Omanik = new SelectList(db.Kasutaja, "Kood", "Email", eksemplar.Omanik);
            return View(eksemplar);
        }

        public ActionResult LisaSilt(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            int eksId = id.Value / 1000; //Üle 1000 sildi vaevalt, et keegi endale teeb
            int siltId = id.Value % 1000;
            db.EksemplariSilt.Add(new EksemplariSilt { EksemplariKood = eksId, SildiKood = siltId });
            db.SaveChanges();
            return RedirectToAction($"Edit/{eksId}");
        }

        public ActionResult EemaldaSilt(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            int eksId = id.Value / 1000;
            int siltId = id.Value % 1000;

            EksemplariSilt es = db.EksemplariSilt.Where(x => x.EksemplariKood == eksId && x.SildiKood == siltId).SingleOrDefault();
            if (es != null)
            {
                db.EksemplariSilt.Remove(es);
                db.SaveChanges();
            }

            return RedirectToAction($"Edit/{eksId}");
        }

        [Authorize]
        // GET: Eksemplars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            if (eksemplar == null)
            {
                return HttpNotFound();
            }
            return View(eksemplar);
        }

        [Authorize]
        // POST: Eksemplars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Eksemplar eksemplar = db.Eksemplar.Find(id);
            db.Eksemplar.Remove(eksemplar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
