﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;

namespace RaamatukoguAPP11.Controllers
{
    public class SiltsController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();
        [Authorize]
        // GET: Silts
        public ActionResult Index()
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            var silt = db.Silt.Include(s => s.Kasutaja);
            if (Kasutaja.ByEmail(User.Identity.Name) != null)
                return View(silt.Where(x=>x.KasutajaKood==k.Kood).ToList().OrderBy(x=>x.Silt1));
            else
             return View(silt.ToList());
        }
        [Authorize]
        // GET: Silts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Silt silt = db.Silt.Find(id);
            if (silt == null)
            {
                return HttpNotFound();
            }
            return View(silt);
        }
        [Authorize]
        // GET: Silts/Create
        public ActionResult Create()
        {
            ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email");
            return View();
        }

        // POST: Silts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Silt1,KasutajaKood")] Silt silt)
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            silt.KasutajaKood = ka.Kood;
            if (ModelState.IsValid)
            {
                db.Silt.Add(silt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", silt.KasutajaKood);
            return View(silt);
        }
        [Authorize]
        // GET: Silts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Silt silt = db.Silt.Find(id);
            if (silt == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", silt.KasutajaKood);
            return View(silt);
        }

        // POST: Silts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Silt1,KasutajaKood")] Silt silt)
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            silt.KasutajaKood = ka.Kood;
            if (ModelState.IsValid)
            {
                db.Entry(silt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", silt.KasutajaKood);
            return View(silt);
        }

        // GET: Silts/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Silt silt = db.Silt.Find(id);
            if (silt == null)
            {
                return HttpNotFound();
            }
            return View(silt);
        }

        // POST: Silts/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Silt silt = db.Silt.Find(id);
            db.Silt.Remove(silt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
