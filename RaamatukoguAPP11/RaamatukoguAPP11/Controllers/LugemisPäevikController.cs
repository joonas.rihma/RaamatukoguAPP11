﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;

namespace RaamatukoguAPP11.Controllers
{
    public class LugemisPäevikController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();
        [Authorize]
        // GET: LugemisPäevik
        public ActionResult Index()
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);

            var lugemispäevik = db.LugemisPäevik.Include(l => l.Kasutaja).Include(l => l.Raamat);

            if (Kasutaja.ByEmail(User.Identity.Name) != null)
                return View(lugemispäevik.Where(x => x.LugejaKood == k.Kood).ToList().OrderBy(x => x.Raamat.Tiitel));
            else
                return View(lugemispäevik.ToList().OrderBy(x => x.Raamat.Tiitel));

            
            //return View(lugemisPäevik.ToList());
        }

        [Authorize]
        // GET: LugemisPäevik/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LugemisPäevik lugemisPäevik = db.LugemisPäevik.Find(id);
            if (lugemisPäevik == null)
            {
                return HttpNotFound();
            }
            return View(lugemisPäevik);
        }

        [Authorize]
        // GET: LugemisPäevik/Create
        public ActionResult Create()
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            ViewBag.LugejaKood = new SelectList(db.Kasutaja, "Kood", "Email", ka.Kood);
            ViewBag.RaamatuKood = new SelectList(db.Raamat.OrderBy(x => x.Tiitel), "Kood", "Tiitel");
            return View();
        }

        [Authorize]
        // POST: LugemisPäevik/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,LugejaKood,RaamatuKood,Staatus,Hinnang,Kuupäev,Kommentaar")] LugemisPäevik lugemisPäevik)
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            lugemisPäevik.LugejaKood = ka.Kood;
            if (ModelState.IsValid)
            {
                db.LugemisPäevik.Add(lugemisPäevik);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            
            ViewBag.LugejaKood = new SelectList(db.Kasutaja, "Kood", "Email", ka.Kood);
            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "RaamatuKood", lugemisPäevik.RaamatuKood);
            return View(lugemisPäevik);
        }

        [Authorize]
        // GET: LugemisPäevik/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LugemisPäevik lugemisPäevik = db.LugemisPäevik.Find(id);
            if (lugemisPäevik == null)
            {
                return HttpNotFound();
            }
            ViewBag.LugejaKood = new SelectList(db.Kasutaja, "Kood", "Email", lugemisPäevik.LugejaKood);
            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "Tiitel", lugemisPäevik.RaamatuKood);
            return View(lugemisPäevik);
        }

        [Authorize]
        // POST: LugemisPäevik/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,LugejaKood,RaamatuKood,Staatus,Hinnang,Kuupäev,Kommentaar")] LugemisPäevik lugemisPäevik)
        {
            Kasutaja ka = Kasutaja.ByEmail(User.Identity.Name);
            lugemisPäevik.LugejaKood = ka.Kood;
            if (ModelState.IsValid)
            {
                db.Entry(lugemisPäevik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LugejaKood = new SelectList(db.Kasutaja, "Kood", "Email", lugemisPäevik.LugejaKood);
            ViewBag.RaamatuKood = new SelectList(db.Raamat, "Kood", "Tiitel", lugemisPäevik.RaamatuKood);
            return View(lugemisPäevik);
        }

        [Authorize]
        // GET: LugemisPäevik/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LugemisPäevik lugemisPäevik = db.LugemisPäevik.Find(id);
            if (lugemisPäevik == null)
            {
                return HttpNotFound();
            }
            return View(lugemisPäevik);
        }

        [Authorize]
        // POST: LugemisPäevik/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LugemisPäevik lugemisPäevik = db.LugemisPäevik.Find(id);
            db.LugemisPäevik.Remove(lugemisPäevik);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
