﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;
using RaamatukoguAPP11.Models;

namespace RaamatukoguAPP11
{

    partial class Kasutaja
    {
        static RaamatukoguEntities db = new RaamatukoguEntities();

        public static Kasutaja OldByEmail(string email)
        {

            return db.Kasutaja.Where(x => x.Email == email).Take(1).SingleOrDefault();

        }

        public static Kasutaja ByEmail(string email)
        {

            Kasutaja kasutaja = OldByEmail(email);
            if (kasutaja == null)
            {

                db.Kasutaja.Add(kasutaja = new Kasutaja { Email = email, Nimi = "Lisa nimi" });
                db.SaveChanges();
                db.KasutajaInRoll.Add(new KasutajaInRoll { KasutajaKood = kasutaja.Kood, RolliKood = 4 });
                db.SaveChanges();


            }
            return kasutaja;
        }
        public static Kasutaja ByKood(int id)
        {
            return db.Kasutaja.Where(x => x.Kood == id).Take(1).SingleOrDefault();
        }

    }
}


namespace RaamatukoguAPP11.Controllers
{
    public class KasutajasController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();
        // GET: Kasutajas
        [Authorize]
        public ActionResult Index(int? id)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Home");
            
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                return View(db.Kasutaja);
            }
                if (Kasutaja.ByEmail(User.Identity.Name) != null)
                return View(db.Kasutaja.Where(x=>x.Kood==k.Kood).ToList().OrderBy(x=>x.Nimi));
            else
                return RedirectToAction("Index", "Home");



        }
        [Authorize]
        // GET: Kasutajas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }

            return View(kasutaja);
        }
        [Authorize]
        // GET: Kasutajas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kasutajas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Email,Nimi")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Kasutaja.Add(kasutaja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kasutaja);
        }
        [Authorize]
        // GET: Kasutajas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }
        [Authorize]
        // POST: Kasutajas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Email,Nimi")] Kasutaja kasutaja)
        {
  
            
                if (ModelState.IsValid)
                {
                    db.Entry(kasutaja).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction($"Edit/{kasutaja.Kood}");
                }
             
            return View(kasutaja);
        }
        [Authorize]
        public ActionResult LisaRoll (int? id)
        {

            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            int kasutajaID = id.Value / 1000;
            int rollID = id.Value % 1000;
            db.KasutajaInRoll.Add(new KasutajaInRoll { KasutajaKood = kasutajaID, RolliKood = rollID });
            db.SaveChanges();
            return RedirectToAction($"Edit/{kasutajaID}");

        }
        [Authorize]
        public ActionResult EemaldaRoll( int? id)
        {
            if (id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            int kasutajaID = id.Value / 1000;
            int rollID = id.Value % 1000;

            KasutajaInRoll roll = db.KasutajaInRoll.Where(x => x.KasutajaKood == kasutajaID && x.RolliKood == rollID).SingleOrDefault();
            if (roll !=null)
            {
                db.KasutajaInRoll.Remove(roll);
                db.SaveChanges();
            }

            return RedirectToAction($"Edit/{kasutajaID}");

        }
        [Authorize]
        // GET: Kasutajas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }
        [Authorize]
        // POST: Kasutajas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            db.Kasutaja.Remove(kasutaja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
