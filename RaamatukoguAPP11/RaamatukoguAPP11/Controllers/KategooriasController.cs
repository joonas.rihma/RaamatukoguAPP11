﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;

namespace RaamatukoguAPP11.Controllers
{
    public class KategooriasController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();
        [Authorize]
        // GET: Kategoorias
        public ActionResult Index()
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            { return View(db.Kategooria.ToList().Skip(1).OrderBy(x => x.Kategooria1)); }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            { return View(db.Kategooria.ToList().Skip(1).OrderBy(x => x.Kategooria1)); }
            else
                return RedirectToAction("Index", "Home");
        }
        [Authorize]
        // GET: Kategoorias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategooria kategooria = db.Kategooria.Find(id);
            if (kategooria == null)
            {
                return HttpNotFound();
            }
            return View(kategooria);
        }
        [Authorize]
        // GET: Kategoorias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kategoorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Kategooria1")] Kategooria kategooria)
        {
            if (ModelState.IsValid)
            {
                Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
                if (k == null) return RedirectToAction("Index", "Home");
                if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
                {
                    db.Kategooria.Add(kategooria);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
                {
                    db.Kategooria.Add(kategooria);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        // GET: Kategoorias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategooria kategooria = db.Kategooria.Find(id);
            if (kategooria == null)
            {
                return HttpNotFound();
            }
            return View(kategooria);
        }
        [Authorize]
        // POST: Kategoorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Kategooria1")] Kategooria kategooria)
        {
            if (ModelState.IsValid)
            {
                Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
                if (k == null) return RedirectToAction("Index", "Home");
                if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
                {
                    db.Entry(kategooria).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
                {
                    db.Entry(kategooria).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        // GET: Kategoorias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategooria kategooria = db.Kategooria.Find(id);
            if (kategooria == null)
            {
                return HttpNotFound();
            }
            return View(kategooria);
        }
        [Authorize]
        // POST: Kategoorias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                Kategooria kategooria = db.Kategooria.Find(id);
                db.Kategooria.Remove(kategooria);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                Kategooria kategooria = db.Kategooria.Find(id);
                db.Kategooria.Remove(kategooria);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            else
                return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
