﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;

namespace RaamatukoguAPP11
{

    partial class KasutajaInRoll
    {

        static RaamatukoguEntities db = new RaamatukoguEntities();

        public static KasutajaInRoll ByKood(int kood)
        {
            return db.KasutajaInRoll.Where(x => x.RolliKood==kood).Take(1).SingleOrDefault();

        }

    }

    //partial class Roll
    //{
    //    static RaamatukoguEntities db = new RaamatukoguEntities();

    //    public static Roll ByKood(int kood)
    //    {

    //        return db.Roll.Where(x => x.Kood == kood).Take(1).SingleOrDefault();

    //    }

    //}
}



namespace RaamatukoguAPP11.Controllers
{
 
    public class KasutajaInRollsController : Controller
    {
        
        private RaamatukoguEntities db = new RaamatukoguEntities();
        //GET: KasutajaInRolls
        [Authorize]
        public ActionResult Index()
        {
            Kasutaja kasutaja = Kasutaja.ByEmail(User.Identity.Name);
            if (kasutaja == null) return RedirectToAction("Index", "Home");
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                var kasutajaInRoll = db.KasutajaInRoll.Include(k => k.Kasutaja).Include(k => k.Roll);
                return View(kasutajaInRoll.ToList());
            }
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                var kasutajaInRoll = db.KasutajaInRoll.Include(k => k.Kasutaja).Include(k => k.Roll);
                return View(kasutajaInRoll.ToList());
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [Authorize]
        // GET: KasutajaInRolls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            if (kasutajaInRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaInRoll);
        }
        [Authorize]
        // GET: KasutajaInRolls/Create
        public ActionResult Create()
        {
            Kasutaja kasutaja = Kasutaja.ByEmail(User.Identity.Name);
            if (kasutaja == null) return RedirectToAction("Index", "Home");
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email");
                ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus");
                return View();
            }
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email");
                ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus");
                return View();
            }

            else
                return RedirectToAction("Index", "Home");
        }

        // POST: KasutajaInRolls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,KasutajaKood,RolliKood")] KasutajaInRoll kasutajaInRoll)
        {
            if (ModelState.IsValid)
            {
                Kasutaja kasutaja = Kasutaja.ByEmail(User.Identity.Name);
                if (kasutaja == null) return RedirectToAction("Index", "Home");
                if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
                {
                    db.KasutajaInRoll.Add(kasutajaInRoll);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
                {
                    db.KasutajaInRoll.Add(kasutajaInRoll);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    return RedirectToAction("Index", "Home");

            }

            ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", kasutajaInRoll.KasutajaKood);
            ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus", kasutajaInRoll.RolliKood);
            return View(kasutajaInRoll);
        }
        [Authorize]
        // GET: KasutajaInRolls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            if (kasutajaInRoll == null)
            {
                return HttpNotFound();
            }
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", kasutajaInRoll.KasutajaKood);
                ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus", kasutajaInRoll.RolliKood);
                return View(kasutajaInRoll);
            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", kasutajaInRoll.KasutajaKood);
                ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus", kasutajaInRoll.RolliKood);
                return View(kasutajaInRoll);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        // POST: KasutajaInRolls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,KasutajaKood,RolliKood")] KasutajaInRoll kasutajaInRoll)
        {
            Kasutaja kasutaja = Kasutaja.ByEmail(User.Identity.Name);
            if (kasutaja == null) return RedirectToAction("Index", "Home");
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                if (ModelState.IsValid)
                {
                    db.Entry(kasutajaInRoll).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", kasutajaInRoll.KasutajaKood);
                ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus", kasutajaInRoll.RolliKood);
                return View(kasutajaInRoll);
            }
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                if (ModelState.IsValid)
                {
                    db.Entry(kasutajaInRoll).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.KasutajaKood = new SelectList(db.Kasutaja, "Kood", "Email", kasutajaInRoll.KasutajaKood);
                ViewBag.RolliKood = new SelectList(db.Roll, "Kood", "Nimetus", kasutajaInRoll.RolliKood);
                return View(kasutajaInRoll);
            }
            else
                return RedirectToAction("Index", "Home");
        }
        [Authorize]
        // GET: KasutajaInRolls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            if (kasutajaInRoll == null)
            {
                return HttpNotFound();
            }
            Kasutaja kasutaja = Kasutaja.ByEmail(User.Identity.Name);
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                return View(kasutajaInRoll);
            }
            if (kasutaja.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                return View(kasutajaInRoll);
            }
            else
                return RedirectToAction("Index", "Home");
        }
        [Authorize]
        // POST: KasutajaInRolls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaInRoll kasutajaInRoll = db.KasutajaInRoll.Find(id);
            db.KasutajaInRoll.Remove(kasutajaInRoll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
