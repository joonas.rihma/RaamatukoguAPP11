﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;
using System.IO;
using Newtonsoft.Json;



namespace RaamatukoguAPP11.Controllers
{

    public class RaamatsController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();
        [Authorize]
        public ActionResult RaamatuPilt(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Raamat r = db.Raamat.Find(id.Value);
            if (r == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (r.Pilt?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var pilt = r.Pilt.ToArray();
            return File(pilt, "image/jpg");
        }

        //public string Raamat(int? id)//See oli see API teema vist
        //{
        //    if (id == null) return null;
        //    Raamat r = db.Raamat.Find(id.Value);
        //    if (r == null) return null;
        //    string s = JsonConvert.SerializeObject(new { r.Kood, r.Isbn, r.Tiitel, r.Autor, r.Aasta, r.Kategooria.Kategooria1 });
        //    return s;
        //}

        // GET: Raamats
        //public ActionResult Index()
        //{
        //    return View(db.Raamat.ToList().Skip(1).OrderBy(x=>x.Tiitel));//Skipib nullraamatu ja paneb tähestiku järjekorda
        //}
        [Authorize]
        public ActionResult Index(string searching)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                return View(db.Raamat.Where(x => (x.Tiitel.Contains(searching)
            || x.Autor.Contains(searching)
            || x.Kategooria.Kategooria1.Contains(searching))
            || searching == null).ToList().SkipWhile(y => y.Kood == 0).OrderBy(y => y.Tiitel));
            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                return View(db.Raamat.Where(x => (x.Tiitel.Contains(searching)
                || x.Autor.Contains(searching)
                || x.Kategooria.Kategooria1.Contains(searching))
                || searching == null).ToList().SkipWhile(y => y.Kood == 0).OrderBy(y => y.Tiitel));
            }
            else
                return RedirectToAction("Index", "Home");

        }
        [Authorize]
        // GET: Raamats/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Raamat raamat = db.Raamat.Find(id);
            if (raamat == null)
            {
                return HttpNotFound();
            }
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                return View(raamat);
            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                return View(raamat);
            }

            else
                return RedirectToAction("Index", "Home");

        }

        // GET: Raamats/Create
        [Authorize]
        public ActionResult Create(string ISBN)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                Raamat raamat = new Raamat();
                if (ISBN != null)
                {
                    raamat.Isbn = ISBN;

                }

                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1");

                return View("Create", raamat);
            }

            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                Raamat raamat = new Raamat();
                if (ISBN != null)
                {
                    raamat.Isbn = ISBN;

                }

                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1");

                return View("Create", raamat);
            }

            else
                return RedirectToAction("Index", "Home");

        }
        [Authorize]
        public ActionResult GenereeriIsbn()
        {

            Random random = new Random();
            string r = "";
                       
            for (int i = 1; i < 13; i++)
            {
                r += random.Next(0, 9).ToString();
            }
          
            return Create("x"+r);

        }
        [Authorize]

        // POST: Raamats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,Isbn,Tiitel,Autor,Aasta,Pilt,KategooriaKood")] Raamat raamat, HttpPostedFileBase file)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                if (ModelState.IsValid)
                {

                    if (file != null && file.ContentLength > 0)
                    {
                        using (BinaryReader br = new BinaryReader(file.InputStream))
                        {
                            byte[] buff = br.ReadBytes(file.ContentLength);
                            raamat.Pilt = buff;
                        }
                    }
                    db.Raamat.Add(raamat);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", raamat.KategooriaKood);
                return View(raamat);
            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                if (ModelState.IsValid)
                {

                    if (file != null && file.ContentLength > 0)
                    {
                        using (BinaryReader br = new BinaryReader(file.InputStream))
                        {
                            byte[] buff = br.ReadBytes(file.ContentLength);
                            raamat.Pilt = buff;
                        }
                    }
                    db.Raamat.Add(raamat);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", raamat.KategooriaKood);
                return View(raamat);
            }
            else
                return RedirectToAction("Index", "Home");

        }

        [Authorize]
        // GET: Raamats/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Raamat raamat = db.Raamat.Find(id);
            if (raamat == null)
            {
                return HttpNotFound();
            }
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", raamat.KategooriaKood);
                return View(raamat);
            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", raamat.KategooriaKood);
                return View(raamat);
            }
            else
                return RedirectToAction("Index", "Home");


        }
        [Authorize]
        // POST: Raamats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,Isbn,Tiitel,Autor,Aasta,Pilt,KategooriaKood")] Raamat raamat, HttpPostedFileBase file)
        {
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Admin"))
            {
                if (ModelState.IsValid)
                {
                    db.Entry(raamat).State = EntityState.Modified;
                    db.Entry(raamat).Property(x => x.Pilt).IsModified = false;
                    if (file != null && file.ContentLength > 0)
                    {
                        using (BinaryReader br = new BinaryReader(file.InputStream))
                        {
                            byte[] buff = br.ReadBytes(file.ContentLength);
                            raamat.Pilt = buff;
                        }
                    }

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", raamat.KategooriaKood);
                return View(raamat);
            }
            if (k.KasutajaInRoll.Select(x => x.Roll.Nimetus).Contains("Moderaator"))
            {
                if (ModelState.IsValid)
                {
                    db.Entry(raamat).State = EntityState.Modified;
                    db.Entry(raamat).Property(x => x.Pilt).IsModified = false;
                    if (file != null && file.ContentLength > 0)
                    {
                        using (BinaryReader br = new BinaryReader(file.InputStream))
                        {
                            byte[] buff = br.ReadBytes(file.ContentLength);
                            raamat.Pilt = buff;
                        }
                    }

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.KategooriaKood = new SelectList(db.Kategooria.OrderBy(x => x.Kategooria1), "Kood", "Kategooria1", raamat.KategooriaKood);
                return View(raamat);
            }
            else
                return RedirectToAction("Index", "Home");

        }
        [Authorize]
        // GET: Raamats/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Raamat raamat = db.Raamat.Find(id);
            if (raamat == null)
            {
                return HttpNotFound();
            }
            return View(raamat);
        }
        [Authorize]
        // POST: Raamats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Raamat raamat = db.Raamat.Find(id);
            db.Raamat.Remove(raamat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
