﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaamatukoguAPP11;

namespace RaamatukoguAPP11.Controllers
{
    public class EksemplariSiltsController : Controller
    {
        private RaamatukoguEntities db = new RaamatukoguEntities();

        // GET: EksemplariSilts
        public ActionResult Index()
        {
            
            var eksemplariSilt = db.EksemplariSilt.Include(e => e.Eksemplar).Include(e => e.Silt);
                return View(eksemplariSilt.ToList());
        }

        // GET: EksemplariSilts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EksemplariSilt eksemplariSilt = db.EksemplariSilt.Find(id);
            if (eksemplariSilt == null)
            {
                return HttpNotFound();
            }
            return View(eksemplariSilt);
        }

        // GET: EksemplariSilts/Create
        public ActionResult Create()
        {
            ViewBag.EksemplariKood = new SelectList(db.Eksemplar, "Kood", "AsukohaKommentaar");
            ViewBag.SildiKood = new SelectList(db.Silt, "Kood", "Silt1");
            return View();
        }

        // POST: EksemplariSilts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Kood,EksemplariKood,SildiKood")] EksemplariSilt eksemplariSilt)
        {
            if (ModelState.IsValid)
            {
                db.EksemplariSilt.Add(eksemplariSilt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EksemplariKood = new SelectList(db.Eksemplar, "Kood", "AsukohaKommentaar", eksemplariSilt.EksemplariKood);
            ViewBag.SildiKood = new SelectList(db.Silt, "Kood", "Silt1", eksemplariSilt.SildiKood);
            return View(eksemplariSilt);
        }

        // GET: EksemplariSilts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EksemplariSilt eksemplariSilt = db.EksemplariSilt.Find(id);
            if (eksemplariSilt == null)
            {
                return HttpNotFound();
            }
            ViewBag.EksemplariKood = new SelectList(db.Eksemplar, "Kood", "AsukohaKommentaar", eksemplariSilt.EksemplariKood);
            ViewBag.SildiKood = new SelectList(db.Silt, "Kood", "Silt1", eksemplariSilt.SildiKood);
            return View(eksemplariSilt);
        }

        // POST: EksemplariSilts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Kood,EksemplariKood,SildiKood")] EksemplariSilt eksemplariSilt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eksemplariSilt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EksemplariKood = new SelectList(db.Eksemplar, "Kood", "AsukohaKommentaar", eksemplariSilt.EksemplariKood);
            ViewBag.SildiKood = new SelectList(db.Silt, "Kood", "Silt1", eksemplariSilt.SildiKood);
            return View(eksemplariSilt);
        }

        // GET: EksemplariSilts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EksemplariSilt eksemplariSilt = db.EksemplariSilt.Find(id);
            if (eksemplariSilt == null)
            {
                return HttpNotFound();
            }
            return View(eksemplariSilt);
        }

        // POST: EksemplariSilts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EksemplariSilt eksemplariSilt = db.EksemplariSilt.Find(id);
            db.EksemplariSilt.Remove(eksemplariSilt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
