﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RaamatukoguAPP11.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page";

            return View();
        }

        public ActionResult Contact()
        {
           // ViewBag.Message = "Your contact page";

            return View();
        }

        public ActionResult Muu()
        {
            ViewBag.Message = "Admini ja mode lehekene";

            return View();
        }

        public ActionResult Kasutaja()
        {
            ViewBag.Message = "Tavakasutaja lehekene";

            return View();
        }

        public ActionResult Leheke()
        {
            ViewBag.Message = "Üksi lendav leheke";

            return View();
        }
    }
}